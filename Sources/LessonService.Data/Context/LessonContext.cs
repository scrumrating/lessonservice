﻿using LessonService.Core.DTO;
using Microsoft.EntityFrameworkCore;

namespace LessonService.Data
{
    /// <summary>
    /// Контекст базы данных.
    /// </summary>
    public class LessonContext : DbContext
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="LessonContext"/>.
        /// </summary>
        public LessonContext(DbContextOptions options) : base(options)
        {
        }

        /// <summary>
        /// Получает или задает содержимое базы данных Placeholder.
        /// </summary>
        public DbSet<LessonDto> Lessons { get; set; }
    }
}