﻿using System;
using LessonService.Consumers.Create;
using LessonService.Consumers.Delete;
using LessonService.Consumers.GetAll;
using LessonService.Consumers.GetAllpagination;
using LessonService.Consumers.GetById;
using LessonService.Consumers.Healthchecks;
using LessonService.Consumers.Update;
using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace LessonService.Instance.Infrastructure.MessageBroker
{
    /// <summary>
    /// Регистрация consumers в приложении.
    /// </summary>
    internal static class MessageBrokerRegistration
    {
        /// <summary>
        /// Регистрирует получателй броекра сообщений.
        /// </summary>
        /// <param name="services">Список сервисов.</param>
        /// <returns>Изменённый список сервисов.</returns>
        public static IServiceCollection ConsumerRegistration(this IServiceCollection services)
        {
            services.AddSingleton<CreateConsumer>();
            services.AddSingleton<DeleteConsumer>();
            services.AddSingleton<GetAllConsumer>();
            services.AddSingleton<GetAllPaginationConsumer>();
            services.AddSingleton<GetByIdConsumer>();
            services.AddSingleton<UpdateConsumer>();
            return services;
        }

        
        /// <summary>
        /// Регистрирует броекр сообщений.
        /// </summary>
        /// <param name="services">Список сервисов.</param>
        /// <returns>Изменённый список сервисов.</returns>
        public static IServiceCollection BusRegistration(this IServiceCollection services, IConfiguration configuration)
        {
            // Регистрация потребителей сообщений
            services.AddMassTransit(x =>
            {
                x.AddConsumer<HealthcheckConsumer>();
                x.AddConsumer<CreateConsumer>();
                x.AddConsumer<DeleteConsumer>();
                x.AddConsumer<GetAllConsumer>();
                x.AddConsumer<GetAllPaginationConsumer>();
                x.AddConsumer<GetByIdConsumer>();
                x.AddConsumer<UpdateConsumer>();
            });

            // Регистрация шины.
            // Подробнее про регистрацию шины можно почитать здесь: https://masstransit-project.com/usage/
            services.AddSingleton(serviceProvider => Bus.Factory.CreateUsingRabbitMq(configure =>
            {
                var busConfiguration = configuration.GetSection("Bus").Get<BusConfiguration>();

                // Конфигурация подключения к шине, включающая в себя указание адреса и учетных данных.
                configure.Host(new Uri(busConfiguration.ConnectionString), host =>
                {
                    host.Username(busConfiguration.Username);
                    host.Password(busConfiguration.Password);

                    // Подтверждение получения гарантирует доставку сообщений за счет ухудшения производительности.
                    host.PublisherConfirmation = busConfiguration.PublisherConfirmation;
                });

                // Добавление Serilog для журналирования внутренностей MassTransit.
                configure.UseSerilog();

                // Регистрация очередей и их связи с потребителями сообщений.
                // В качестве метки сообщения используется полное имя класса сообщения, которое потребляет потребитель.

                configure.ReceiveEndpoint(typeof(HealthcheckCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<HealthcheckConsumer>(serviceProvider);
                    EndpointConvention.Map<HealthcheckCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(CreateCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<CreateConsumer>(serviceProvider);
                    EndpointConvention.Map<CreateCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(DeleteCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<DeleteConsumer>(serviceProvider);
                    EndpointConvention.Map<DeleteCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(GetAllCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<GetAllConsumer>(serviceProvider);
                    EndpointConvention.Map<GetAllCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(GetAllPaginationCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<GetAllPaginationConsumer>(serviceProvider);
                    EndpointConvention.Map<GetAllPaginationCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(GetByIdCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<GetByIdConsumer>(serviceProvider);
                    EndpointConvention.Map<GetByIdCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(UpdateCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<UpdateConsumer>(serviceProvider);
                    EndpointConvention.Map<UpdateCommand>(endpoint.InputAddress);
                });
            }));

            // Регистрация сервисов MassTransit.
            services.AddSingleton<IPublishEndpoint>(
                serviceProvider => serviceProvider.GetRequiredService<IBusControl>());
            services.AddSingleton<ISendEndpointProvider>(serviceProvider =>
                serviceProvider.GetRequiredService<IBusControl>());
            services.AddSingleton<IBus>(serviceProvider => serviceProvider.GetRequiredService<IBusControl>());

            // Регистрация клиентов для запроса данных от потребителей сообщений из api.
            // Каждый клиент зарегистрирован таким образом, что бы в рамках каждого запроса к api существовал свой клиент.

            services.AddScoped(serviceProvider =>
                serviceProvider.GetRequiredService<IBus>().CreateRequestClient<HealthcheckCommand>());
            services.AddScoped(serviceProvider =>
                serviceProvider.GetRequiredService<IBus>().CreateRequestClient<CreateCommand>());
            services.AddScoped(serviceProvider =>
                serviceProvider.GetRequiredService<IBus>().CreateRequestClient<DeleteCommand>());
            services.AddScoped(serviceProvider =>
                serviceProvider.GetRequiredService<IBus>().CreateRequestClient<GetAllCommand>());
            services.AddScoped(serviceProvider =>
                serviceProvider.GetRequiredService<IBus>().CreateRequestClient<GetAllPaginationCommand>());
            services.AddScoped(serviceProvider =>
                serviceProvider.GetRequiredService<IBus>().CreateRequestClient<GetByIdCommand>());
            services.AddScoped(serviceProvider =>
                serviceProvider.GetRequiredService<IBus>().CreateRequestClient<UpdateCommand>());

            // Регистрация сервиса шины MassTransit.
            services.AddSingleton<IHostedService, BusService>();
            return services;
        }
    }
}