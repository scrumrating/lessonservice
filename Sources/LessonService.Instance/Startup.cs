﻿using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using LessonService.Consumers;
using LessonService.Consumers.Healthchecks;
using LessonService.Core;
using LessonService.Core.Services;
using LessonService.Data;
using LessonService.Data.Repositories;
using LessonService.WebApi;
using System;
using LessonService.Consumers.Infrastructure;
using LessonService.Core.DTO;
using LessonService.Core.Entities;
using LessonService.Core.Infrastructure;
using LessonService.Core.Interfaces;
using LessonService.Data.Infrastructure;
using LessonService.Instance.Infrastructure.MessageBroker;
using LessonService.Instance.Infrastructure.Swagger;
using Microsoft.EntityFrameworkCore;

namespace LessonService.Instance
{
    /// <summary>
    /// Инициализация приложения.
    /// </summary>
    public class Startup
    {
        private readonly IConfiguration configuration;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Startup"/>.
        /// </summary>
        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            this.configuration = configuration;
            this.ConfigureDataStorage();
        }

        /// <summary>
        /// Конфигурация приложения.
        /// </summary>
        /// <param name="application">Приложение.</param>
        public void Configure(IApplicationBuilder application)
        {
            application.UseCors("AllowAll");
            application.UseMvc();
            application.UseSwaggerDocumentation(this.configuration);
        }

        /// <summary>
        /// Конфигурация сервисов.
        /// </summary>
        /// <param name="services">Сервисы.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(this.configuration)
                .CreateLogger();

            Log.Information("Начинается регистрация политик CORS.");

            services.AddCors(o => o.AddPolicy("AllowAll", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            Log.Information("Регистрация политик CORS успешно завершена.");

            Log.Information("Начинается регистрация Swagger генератора.");

            services.AddSwaggerDocumentation(this.configuration);

            Log.Information("Регистрация Swagger генератора завершена.");

            Log.Information("Начинается регистрация сервисов.");

            services.AddScoped<DbContext, LessonContext>();
            services.AddDbContext<LessonContext>(option =>
                option.UseNpgsql(this.configuration.GetConnectionString("Lessons")));

            //TODO: разобратся с этим дерьмом
            services.AddScoped<IServiceAsync<Lesson>, LessonServiceAsync>();
            services.AddScoped<IRepositoryAsync<LessonDto>, EFRepository<LessonDto>>();

            //services.AddCore();
            
            //services.AddData();

            services.AddConsumers();

            Log.Information("Регистрация сервисов успешно завершена.");

            Log.Information("Начинается регистрация шины.");

            services.BusRegistration(this.configuration);

            Log.Information("Регистрация шины успешно завершена.");

            Log.Information("Начинается регистрация сервисов MVC.");

            services.AddMvc();

            Log.Information("Регистрация сервисов MVC успешно завершена.");
        }

        /// <summary>
        /// Конфигурирует базу данных.
        /// </summary>
        public void ConfigureDataStorage()
        {
            Log.Information("Начинается создание/обновление базы данных.");

            //TODO: Сделать чтобы указывали connectionString в одном месте.

            var contextOptions = new DbContextOptionsBuilder<LessonContext>();
            contextOptions.UseNpgsql(this.configuration.GetConnectionString("Lessons"));

            using (var context = new LessonContext(contextOptions.Options))
            {
                context.Database.EnsureCreated();
            }

            Log.Information("Создание/обновление базы данных завершено.");
        }
    }
}