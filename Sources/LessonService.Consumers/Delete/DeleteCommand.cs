﻿﻿namespace LessonService.Consumers.Delete
{
    /// <summary>
    /// Команда для удаления данных о уроке.
    /// </summary>
    public class DeleteCommand
    {
        /// <summary>
        /// Получает или задает идентификатор записи.
        /// </summary>
        public int Id { get; set; }
    }
}