﻿﻿using System.Threading.Tasks;
using LessonService.Core.Constants;
using LessonService.Core.Entities;
using LessonService.Core.Interfaces;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace LessonService.Consumers.Create
{
    /// <summary>
    /// Обработчик сообщения создания.
    /// </summary>
    public class CreateConsumer : IConsumer<CreateCommand>
    {
        private readonly IServiceAsync<Lesson> service;
        private readonly ILogger<CreateConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="CreateConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public CreateConsumer(IServiceAsync<Lesson> service, ILogger<CreateConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<CreateCommand> context)
        {
            this.logger.LogInformation($"Выполняется обработка сообщения создания '{context.Message.Lesson}'");

            var result = await this.service.CreateAsync(context.Message.Lesson);

            if (result)
            {
                await context.RespondAsync(new CreateResponse() {Result = ResponseResult.Success});
                return;
            }

            await context.RespondAsync(new CreateResponse {Result = ResponseResult.NotSuccess});
        }
    }
}