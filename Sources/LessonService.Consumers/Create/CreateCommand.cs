﻿
using LessonService.Core.Entities;

namespace LessonService.Consumers.Create
{
    /// <summary>
    /// Команда для создания урока.
    /// </summary>
    public class CreateCommand
    {
        /// <summary>
        /// Получает или задает данные урока.
        /// </summary>
        public Lesson Lesson { get; set; }
    }
}