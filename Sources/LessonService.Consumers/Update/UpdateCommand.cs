﻿﻿using LessonService.Core.Entities;

namespace LessonService.Consumers.Update
{
    /// <summary>
    /// Команда для обновления данных о уроке.
    /// </summary>
    public class UpdateCommand
    {
        /// <summary>
        /// Получает или задает данные о уроке.
        /// </summary>
        public Lesson Lesson { get; set; }
    }
}