﻿﻿namespace LessonService.Consumers.Update
{
    /// <summary>
    /// Ответ на команду обновления данных о уроке.
    /// </summary>
    public class UpdateResponse
    {
        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}