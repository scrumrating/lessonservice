﻿﻿using System.Threading.Tasks;
using LessonService.Core.Constants;
using LessonService.Core.Entities;
using LessonService.Core.Interfaces;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace LessonService.Consumers.Update
{
    /// <summary>
    /// Обработчик сообщения обновления данных.
    /// </summary>
    public class UpdateConsumer : IConsumer<UpdateCommand>
    {
        private readonly IServiceAsync<Lesson> service;
        private readonly ILogger<UpdateConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="UpdateConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public UpdateConsumer(IServiceAsync<Lesson> service, ILogger<UpdateConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<UpdateCommand> context)
        {
            this.logger.LogInformation($"Выполняется обработка сообщения обновления данных '{context.Message.Lesson}'.");

            await this.service.UpdateAsync(context.Message.Lesson);
            await context.RespondAsync(new UpdateResponse { Result = ResponseResult.Success });
        }
    }
}