﻿﻿using System.Collections.Generic;
using LessonService.Core.Entities;

namespace LessonService.Consumers.GetAllpagination
{
    /// <summary>
    /// Ответ на команду получения списка.
    /// </summary>
    public class GetAllPaginationResponse
    {
        /// <summary>
        /// Получает или задает список.
        /// </summary>
        public IEnumerable<Lesson> Lessons { get; set; }

        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}