﻿﻿namespace LessonService.Consumers.GetAllpagination
{
    /// <summary>
    /// Команда для получения списка уроков.
    /// </summary>
    public class GetAllPaginationCommand
    {
        /// <summary>
        /// Получает или задает размер страницы.
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Получает или задает номер страницы.
        /// </summary>
        public int PageNumber { get; set; }
    }
}