﻿﻿using System.Threading.Tasks;
using LessonService.Core.Constants;
using LessonService.Core.Entities;
using LessonService.Core.Interfaces;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace LessonService.Consumers.GetAllpagination
{
    /// <summary>
    /// Обработчик сообщения получения списка уроков.
    /// </summary>
    public class GetAllPaginationConsumer : IConsumer<GetAllPaginationCommand>
    {
        private readonly IServiceAsync<Lesson> service;
        private readonly ILogger<GetAllPaginationConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="GetAllPaginationConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public GetAllPaginationConsumer(IServiceAsync<Lesson> service, ILogger<GetAllPaginationConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<GetAllPaginationCommand> context)
        {
            this.logger.LogInformation($"Выполняется обработка сообщения получения всех записей с количеством записей: {context.Message.PageSize}, номером страницы: {context.Message.PageNumber}.");

            var lessons = await this.service.GetAsync(context.Message.PageSize, context.Message.PageNumber);

            if (lessons != null)
            {
                await context.RespondAsync(new GetAllPaginationResponse { Lessons = lessons, Result = ResponseResult.Success });
                return;
            }

            await context.RespondAsync(new GetAllPaginationResponse { Result = ResponseResult.NotSuccess });
        }
    }
}