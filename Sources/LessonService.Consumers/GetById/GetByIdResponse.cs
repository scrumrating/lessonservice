﻿﻿using LessonService.Core.Entities;

namespace LessonService.Consumers.GetById
{
    /// <summary>
    /// Ответ на команду получения данных по id.
    /// </summary>
    public class GetByIdResponse
    {
        /// <summary>
        /// Получает или задаёт данные о уроке.
        /// </summary>
        public Lesson Lesson { get; set; }
        
        /// <summary>
        /// Получает или задаёт результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}