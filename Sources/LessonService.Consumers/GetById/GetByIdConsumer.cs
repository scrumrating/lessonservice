﻿﻿using System.Threading.Tasks;
using LessonService.Core.Constants;
using LessonService.Core.Entities;
using LessonService.Core.Interfaces;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace LessonService.Consumers.GetById
{
    /// <summary>
    /// Обработчик сообщения получения информации по Id.
    /// </summary>
    public class GetByIdConsumer : IConsumer<GetByIdCommand>
    {
        private readonly IServiceAsync<Lesson> service;
        private readonly ILogger<GetByIdConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="GetByIdConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public GetByIdConsumer(IServiceAsync<Lesson> service, ILogger<GetByIdConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<GetByIdCommand> context)
        {
            this.logger.LogInformation("Выполняется обработка сообщения получения данных по Id.");

            var lesson = await this.service.FindByIdAsync(context.Message.Id);

            if (lesson != null)
            {
                await context.RespondAsync(new GetByIdResponse { Lesson = lesson, Result = ResponseResult.Success });
                return;
            }

            await context.RespondAsync(new GetByIdResponse { Result = ResponseResult.NotSuccess });
        }
    }
}