﻿﻿namespace LessonService.Consumers.GetById
{
    /// <summary>
    /// Команда для получение данных по id.
    /// </summary>
    public class GetByIdCommand
    {
        /// <summary>
        /// Получает или задает идентификатор.
        /// </summary>
        public int Id { get; set; }
    }
}