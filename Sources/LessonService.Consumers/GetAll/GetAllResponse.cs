﻿﻿using System.Collections;
using System.Collections.Generic;
using LessonService.Core.Entities;

namespace LessonService.Consumers.GetAll
{
    /// <summary>
    /// Ответ на команду получения списка уроков.
    /// </summary>
    public class GetAllResponse
    {
        /// <summary>
        /// Получает или задает список уроков.
        /// </summary>
        public IEnumerable<Lesson> Lessons { get; set; }

        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}