﻿﻿using System.Threading.Tasks;
using LessonService.Core.Constants;
using LessonService.Core.Entities;
using LessonService.Core.Interfaces;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace LessonService.Consumers.GetAll
{
    /// <summary>
    /// Обработчик сообщения получения списка.
    /// </summary>
    public class GetAllConsumer : IConsumer<GetAllCommand>
    {
        private readonly IServiceAsync<Lesson> service;
        private readonly ILogger<GetAllConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="GetAllConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public GetAllConsumer(IServiceAsync<Lesson> service, ILogger<GetAllConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<GetAllCommand> context)
        {
            this.logger.LogInformation("Выполняется обработка сообщения получения всех записей.");

            var lessons = await this.service.GetAsync();

            if (lessons != null)
            {
                await context.RespondAsync(new GetAllResponse { Lessons = lessons, Result = ResponseResult.Success });
                return;
            }

            await context.RespondAsync(new GetAllResponse { Result = ResponseResult.NotSuccess });
        }
    }
}