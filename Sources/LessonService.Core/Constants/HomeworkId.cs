﻿namespace LessonService.Core.Constants
{
    /// <summary>
    /// Идентификаторы домашнего задания.
    /// </summary>
    public static class HomeworkId
    {
        /// <summary>
        /// Задаёт отсутствие домашнего задания.
        /// </summary>
        public const int NoHomework = 0;
    }
}