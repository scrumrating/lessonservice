﻿using System;

namespace LessonService.Core.DTO
{
    public class LessonDto : IdentityDto
    {
        /// <summary>
        /// получает или задаёт название урока.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Получает или задаёт описание урока.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Получает или задаёт дату проведения урока.
        /// </summary>
        public DateTime DateLesson { get; set; }

        /// <summary>
        /// Получает или задаёт идентификатор домашнего задания.
        /// </summary>
        public int HomeworkId { get; set; }

        /// <summary>
        /// Получает или задаёт идентификатор курса.
        /// </summary>
        public int CourseId { get; set; }
        
    }
}