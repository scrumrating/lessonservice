﻿using System;

namespace LessonService.Core.Entities
{
    /// <summary>
    /// Бизнес модель сущности "Урок".
    /// </summary>
    public class Lesson
    {
        /// <summary>
        /// Получает или задаёт идентификатор урока.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// получает или задаёт название урока.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Получает или задаёт описание урока.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Получает или задаёт дату проведения урока.
        /// </summary>
        public DateTime DateLesson { get; set; }

        /// <summary>
        /// Получает или задаёт идентификатор домашнего задания.
        /// </summary>
        public int HomeworkId { get; set; }

        /// <summary>
        /// Получает или задаёт идентификатор курса.
        /// </summary>
        public int CourseId { get; set; }

        /// <summary>
        /// Инициализирует экземпляр класса <see cref="Lesson"/>
        /// </summary>
        public Lesson()
        {
        }

        /// <summary>
        /// Инициализирует экземпляр класса <see cref="Lesson"/>
        /// </summary>
        /// <param name="id">Идентификатор записи.</param>
        /// <param name="name">Название урока.</param>
        /// <param name="description">описание урока.</param>
        /// <param name="dateLesson">Дата проведения урока.</param>
        /// <param name="homeworkId">Идентификатор домашнего задания.</param>
        /// <param name="courseId">Идентификатор курса.</param>
        public Lesson(int id, string name, string description, DateTime dateLesson, int homeworkId, int courseId)
        {
            this.Id = id;
            this.Name = name;
            this.Description = description;
            this.DateLesson = dateLesson;
            this.HomeworkId = homeworkId;
            this.CourseId = courseId;
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            var isEqual = false;
            switch (obj)
            {
                case null:
                    throw new NullReferenceException();

                case Lesson lesson:
                {
                    var compareToObj = lesson;

                    if (compareToObj.Id.CompareTo(this.Id) == 0)
                    {
                        isEqual = true;
                    }

                    break;
                }

                case string name:
                {
                    if (string.Compare(name, this.Name, StringComparison.Ordinal) == 0)
                    {
                        isEqual = true;
                    }

                    break;
                }
            }

            return isEqual;
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return
                $"Id = {this.Id}, Name = {this.Name}, Description = {this.Description}, DateLesson = {this.DateLesson.Date}, HomeworkId = {this.HomeworkId}, CourseId = {this.CourseId}";
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }
    }
}