﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LessonService.Core.DTO;
using LessonService.Core.Entities;
using LessonService.Core.Interfaces;
using Microsoft.Extensions.Logging;

namespace LessonService.Core.Services
{
    /// <summary>
    /// Сервисный объект для управления lesson.
    /// </summary>
    public class LessonServiceAsync : IServiceAsync<Lesson>
    {
        private readonly IRepositoryAsync<LessonDto> repository;
        private readonly ILogger<LessonServiceAsync> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="LessonServiceAsync"/>.
        /// </summary>
        /// <param name="repository">Хранилище данных о студентах.</param>
        /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public LessonServiceAsync(IRepositoryAsync<LessonDto> repository, ILogger<LessonServiceAsync> logger)
        {
            this.repository = repository;
            this.logger = logger;
        }

        /// <inheritdoc />
        public async Task<bool> CreateAsync(Lesson item)
        {
            this.logger.LogInformation($"Выполняется создание {item}.");

            await this.repository.CreateAsync(new LessonDto()
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                DateLesson = item.DateLesson,
                HomeworkId = item.HomeworkId,
                CourseId = item.CourseId,
            });
            return true;
        }

        /// <inheritdoc />
        public async Task<Lesson> FindByIdAsync(int id)
        {
            this.logger.LogInformation($"Выполняется поиск с id {id}.");
            var lesson = await this.repository.FindByIdAsync(id);
            Lesson resultLesson = null;
            if (lesson != null)
            {
                resultLesson = new Lesson()
                {
                    Id = lesson.Id,
                    Name = lesson.Name,
                    Description = lesson.Description,
                    DateLesson = lesson.DateLesson,
                    HomeworkId = lesson.HomeworkId,
                    CourseId = lesson.CourseId,
                };
            }

            return resultLesson;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<Lesson>> GetAsync()
        {
            this.logger.LogInformation($"Выполняется поиск всех записей.");
            var lessons = await this.repository.GetAsync();
            return lessons.Select(item => new Lesson()
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                DateLesson = item.DateLesson,
                HomeworkId = item.HomeworkId,
                CourseId = item.CourseId,
            });
        }

        /// <inheritdoc />
        public async Task<IEnumerable<Lesson>> GetAsync(int pageSize, int pageNumber)
        {
            this.logger.LogInformation($"Выполняется поиск всех записей с количеством {pageSize}, номером страницы {pageNumber}.");
            var lessons = await this.repository.GetAsync(pageSize, pageNumber);
            return lessons.Select(item => new Lesson()
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                DateLesson = item.DateLesson,
                HomeworkId = item.HomeworkId,
                CourseId = item.CourseId,
            });
        }

        /// <inheritdoc />
        public async Task<IEnumerable<Lesson>> GetAsync(Expression<Func<Lesson, bool>> predicate)
        {
            //TODO: Доделать работу с предикатом. Тоесть конвертировать в Dto.
            this.logger.LogInformation($"Выполняется поиск всех записей с условием {predicate.Body}.");
            var lessons = await this.repository.GetAsync();
            return lessons.Select(item => new Lesson()
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                DateLesson = item.DateLesson,
                HomeworkId = item.HomeworkId,
                CourseId = item.CourseId,
            });
        }

        /// <inheritdoc />
        public async Task RemoveAsync(int id)
        {
            this.logger.LogInformation($"Выполняется удаление с Id {id}.");
            await this.repository.RemoveAsync(id);
        }

        /// <inheritdoc />
        public async Task UpdateAsync(Lesson item)
        {
            this.logger.LogInformation($"Выполняется обновление данных {item}.");
            await this.repository.UpdateAsync(new LessonDto()
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                DateLesson = item.DateLesson,
                HomeworkId = item.HomeworkId,
                CourseId = item.CourseId,
            });
        }
    }
}