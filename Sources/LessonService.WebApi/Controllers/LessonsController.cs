﻿﻿using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;
using LessonService.Consumers.Create;
using LessonService.Consumers.Delete;
using LessonService.Consumers.GetAll;
using LessonService.Consumers.GetAllpagination;
using LessonService.Consumers.GetById;
using LessonService.Consumers.Update;
using LessonService.Core.Constants;
using LessonService.Core.Entities;

namespace LessonService.WebApi.Controllers
{
    /// <summary>
    /// Контроллер Lessons.
    /// </summary>
    [Route("/api/lesson")]
    public class LessonsController : Controller
    {
        private readonly IRequestClient<CreateCommand> createClient;
        private readonly IRequestClient<DeleteCommand> deleteClient;
        private readonly IRequestClient<GetAllCommand> getAllClient;
        private readonly IRequestClient<GetAllPaginationCommand> getAllPaginationClient;
        private readonly IRequestClient<GetByIdCommand> getByIdClient;
        private readonly IRequestClient<UpdateCommand> updateClient;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="LessonsController"/>.
        /// </summary>
        /// <param name="createClient">Клиент создания.</param>
        /// <param name="deleteClient">Клиент удаления.</param>
        /// <param name="getAllClient">Клиент получения всех записей.</param>
        /// <param name="getAllPaginationClient">Клиент получения записей с пагинацией.</param>
        /// <param name="getByIdClient">Клиент получения записи по id.</param>
        /// <param name="updateClient">Клиент обновления записи.</param>
        public LessonsController(
            IRequestClient<CreateCommand> createClient,
            IRequestClient<DeleteCommand> deleteClient,
            IRequestClient<GetAllCommand> getAllClient,
            IRequestClient<GetAllPaginationCommand> getAllPaginationClient,
            IRequestClient<GetByIdCommand> getByIdClient,
            IRequestClient<UpdateCommand> updateClient)
        {
            this.createClient = createClient;
            this.deleteClient = deleteClient;
            this.getAllClient = getAllClient;
            this.getAllPaginationClient = getAllPaginationClient;
            this.getByIdClient = getByIdClient;
            this.updateClient = updateClient;
        }

        /// <summary>
        /// Метод для получения доступным методов.
        /// </summary>
        /// <returns>Список доступных методов.</returns>
        [HttpOptions]
        public IActionResult Options()
        {
            this.Response.Headers.Add("Allow", "GET, OPTIONS");
            return this.Ok();
        }

        /// <summary>
        /// Получить список уроков.
        /// </summary>
        /// <returns>Список Lessons.</returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var response = await this.getAllClient.GetResponse<GetAllResponse>(new GetAllCommand());

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return this.Ok(response.Message.Lessons);
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Получить список Lessons. (Пагинация).
        /// </summary>
        /// <param name="pageSize">Количество строк для получения.</param>
        /// <param name="pageNumber">Номер страницы.</param>
        /// <returns>Список Lessons.</returns>
        [HttpGet]
        [Route("All")]
        public async Task<IActionResult> Get([FromQuery] int pageSize, [FromQuery] int pageNumber)
        {
            try
            {
                var command = new GetAllPaginationCommand()
                {
                    PageSize = pageSize,
                    PageNumber = pageNumber,
                };

                var response = await this.getAllPaginationClient.GetResponse<GetAllPaginationResponse>(command);

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return this.Ok(response.Message.Lessons);
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Получить урок по её идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор.</param>
        /// <returns>Объект найденой записи.</returns>
        [HttpGet]
        [Route("GetById")]
        public async Task<IActionResult> Get([FromQuery(Name = "id")] int id)
        {
            try
            {
                var response =
                    await this.getByIdClient.GetResponse<GetByIdResponse>(new GetByIdCommand() { Id = id });

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return this.Ok(response.Message.Lesson);
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Создание урока.
        /// </summary>
        /// <param name="lesson">Объект урок.</param>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Lesson lesson)
        {
            try
            {
                var response =
                    await this.createClient.GetResponse<CreateResponse>(new CreateCommand() { Lesson = lesson });

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return this.Ok();
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Обновление данных о уроке.
        /// </summary>
        /// <param name="lesson">Объект урок.</param>
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] Lesson lesson)
        {
            try
            {
                var response =
                    await this.updateClient.GetResponse<UpdateResponse>(new UpdateCommand() { Lesson = lesson });

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return this.Ok();
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Удаление урока.
        /// </summary>
        /// <param name="id">Идентификатор для удаления.</param>
        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery(Name = "id")] int id)
        {
            try
            {
                var response = await this.deleteClient.GetResponse<DeleteResponse>(
                    new DeleteCommand()
                    {
                        Id = id,
                    });

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return this.Ok();
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }
    }
}